﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    const string HORIZONTAL_AXIS = "Horizontal";
    const string FORWARD_AXIS = "Vertical";
    const string MOUSE_HORIZONTAL = "Mouse X";
    const string MOUSE_VERTICAL = "Mouse Y";
    const string JUMP = "Jump";
    const string DASH = "Fire3";
    const string CROUCH = "Crouch";
    const string SHOOT = "Fire1";
    const string AIM = "Fire2";
    const string ACTIVATE = "Activate";

    //GETTER METHODS
    public static Vector3 GetDirectionFromInput()
    {
        return new Vector3(Input.GetAxisRaw(HORIZONTAL_AXIS), 0, Input.GetAxisRaw(FORWARD_AXIS));
    }

    public static Vector2 GetMouseMovement()
    {
        return new Vector2(Input.GetAxisRaw(MOUSE_HORIZONTAL), Input.GetAxisRaw(MOUSE_VERTICAL));
    }

    public static bool JumpPressed()
    {
        return Input.GetButtonDown(JUMP);
    }

    public static bool JumpHeld()
    {
        return Input.GetButton(JUMP);
    }

    public static bool DashPressed()
    {
        return Input.GetButtonDown(DASH);
    }

    public static bool DashHeld()
    {
        return Input.GetButton(DASH);
    }

    public static bool CrouchPressed()
    {
        return Input.GetButtonDown(CROUCH);
    }

    public static bool CrouchHeld()
    {
        return Input.GetButton(CROUCH);
    }

    public static bool ShootPressed()
    {
        return Input.GetButtonDown(SHOOT);
    }

    public static bool AimHeld()
    {
        return Input.GetButton(AIM);
    }

    public static bool ActivatePressed()
    {
        return Input.GetButtonDown(ACTIVATE);
    }

    public static bool ActivateHeld()
    {
        return Input.GetButton(ACTIVATE);
    }

}
