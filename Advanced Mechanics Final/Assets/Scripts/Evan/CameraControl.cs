﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField]
    private float lookSensitivity = 100f;
    public Transform player;
    private float xRotation;

    // Start is called before the first frame update
    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        xRotation = 0;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        float mouseX = InputManager.GetMouseMovement().x * lookSensitivity * Time.deltaTime;
        float mouseY = InputManager.GetMouseMovement().y * lookSensitivity * Time.deltaTime;

        xRotation += mouseY;
        //Xrotation = Mathf.Clamp(Xrotation, -90f, 90f);

        if (xRotation > 90)
        {
            xRotation = 90.0f;
            mouseY = 0;
            ClampRotation(270.0f);
        }
        else if (xRotation < -90)
        {
            xRotation = -90;
            mouseY = 0;
            ClampRotation(90.0f);
        }

        transform.Rotate(Vector3.left * mouseY);
        //if (player.gameObject.GetComponent<PlayerMovement>().wallExitTracker)
        //{
        //    //
        //    transform.Rotate(Vector3.up * mouseX, Space.Self);
        //}
        //else
        //{
        //    //
        //    player.Rotate(Vector3.up * mouseX);
        //}

        player.Rotate(Vector3.up * mouseX);

    }

    void ClampRotation(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
