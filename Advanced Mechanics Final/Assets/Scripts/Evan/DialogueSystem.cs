﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class DialogueSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject textObject;
    private TextMesh textMesh;
    private StudioEventEmitter audioEmitter;
    private bool shouldSpeak = false;

    private float charTimer = 0;
    [SerializeField]
    private float charDelay = 0.1f;
    private float lineTimer = 0;
    [SerializeField]
    private float lineDelay = 2.0f;
    private bool lineFinished;

    private string currentDisplay= "";
    private int progressTracker = 0;

    public string[] lines;
    private int currentLine = 0;
    private string selectedLine;


    void Start()
    {
        textMesh = textObject.GetComponent<TextMesh>();
        audioEmitter = GetComponent<StudioEventEmitter>();
    }

    void Update()
    {
        selectedLine = lines[currentLine];

        if (shouldSpeak && !lineFinished && charTimer < Time.time)
        {
            //Next letter
            char currentLetter = selectedLine[progressTracker];
            currentDisplay += currentLetter;
            textMesh.text = currentDisplay;
            progressTracker++;

            //Play sound
            string cL = currentLetter.ToString();
            if (cL != " " && cL != "," && cL != "." && cL != "!" && cL != "\"")
            {
                audioEmitter.Play();
            }

            //Delay before next char
            if (cL == ",")
            {
                charTimer = Time.time + charDelay * 2;
            } else
            {
                charTimer = Time.time + charDelay;
            }
            
        }

        UpdateLines();

    }

    private void UpdateLines()
    {
        if (progressTracker >= selectedLine.Length)
        {
            lineFinished = true;
            progressTracker = 0;
            lineTimer = Time.time + lineDelay;
        }

        if (lineFinished && lineTimer < Time.time)
        {
            lineFinished = false;
            currentLine++;

            currentDisplay = "";
        }

        if (currentLine >= lines.Length)
        {
            shouldSpeak = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            shouldSpeak = true;
        }
    }
}
