﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Battery : MonoBehaviour
{

    ParticleSystem pSys;
    StudioEventEmitter audioEmitter;
    List<GameObject> poweredObjects = new List<GameObject>();

    public float chargeAmount = 100;
    [SerializeField]
    private float chargeDrainAmount = 0.1f;
    private bool shutDown = false;

    // Start is called before the first frame update
    void Start()
    {
        audioEmitter = GetComponent<StudioEventEmitter>();

        pSys = GetComponent<ParticleSystem>();
        pSys.Stop();

    }

    // Update is called once per frame
    void Update()
    {
        chargeAmount = Mathf.Clamp(chargeAmount, 0, 100);

        if (poweredObjects.Count > 0)
        {
            DrainEnergy();
        }

        if (chargeAmount <= 0 && !shutDown)
        {
            ShutDown();
        } else if (chargeAmount > 0)
        {
            shutDown = false;
        }

    }

    private void DrainEnergy()
    {
        chargeAmount -= chargeDrainAmount;
    }

    private void ShutDown()
    {
        pSys.Stop();
        audioEmitter.Stop();
        foreach (GameObject i in poweredObjects)
        {
            if (i.tag == "Turret")
            {
                i.GetComponent<TurretAI>().powerLevel--;
            }
            else if (i.tag == "SlowPlate")
            {
                i.GetComponent<SlowPlate>().activated = false;
            }

            poweredObjects.Remove(i.gameObject);
        }

        shutDown = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Turret" && chargeAmount > 0)
        {
            other.gameObject.GetComponent<TurretAI>().powerLevel++;
            poweredObjects.Add(other.gameObject);
            audioEmitter.Play();
            pSys.Play();
            
        } else if (other.gameObject.tag == "MovingWall")
        {
            other.gameObject.GetComponent<MovingWall>().StartMove();
            audioEmitter.Play();
            pSys.Play();
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Turret")
        {
            other.gameObject.GetComponent<TurretAI>().powerLevel--;
            poweredObjects.Remove(other.gameObject);
            audioEmitter.Stop();
            pSys.Stop();
            

        } else if (other.gameObject.tag == "MovingWall")
        {
            //other.gameObject.GetComponent<MovingWall>().shouldMove = false;
            audioEmitter.Stop();
            pSys.Stop();
            

        }

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "SlowPlate" && chargeAmount > 0)
        {
            collision.collider.gameObject.GetComponent<SlowPlate>().activated = true;
            poweredObjects.Add(collision.gameObject);
            audioEmitter.Play();
            pSys.Play();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject.tag == "SlowPlate")
        {
            collision.collider.gameObject.GetComponent<SlowPlate>().activated = false;
            poweredObjects.Remove(collision.gameObject);
            audioEmitter.Stop();
            pSys.Stop();
        }
    }

}
