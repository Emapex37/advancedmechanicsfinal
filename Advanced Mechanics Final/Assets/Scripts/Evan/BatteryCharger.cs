﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryCharger : MonoBehaviour
{
    ParticleSystem pSys;
    List<GameObject> chargingObjects = new List<GameObject>();

    [SerializeField]
    private float rechargeRate = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        pSys = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {

        if (chargingObjects.Count > 0)
        {
            pSys.Play();
            Charge();
        } else
        {
            pSys.Stop();
        }

    }

    private void Charge()
    {
        foreach (GameObject i in chargingObjects)
        {
            i.GetComponent<Battery>().chargeAmount += rechargeRate;
            if (i.GetComponent<Battery>().chargeAmount >= 100)
            {
                i.GetComponent<ParticleSystem>().Play();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.layer == LayerMask.NameToLayer("Battery"))
        {
            chargingObjects.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Battery"))
        {
            chargingObjects.Remove(other.gameObject);
            other.GetComponent<ParticleSystem>().Stop();
        }
    }

}
