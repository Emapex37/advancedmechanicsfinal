﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{

    public bool shouldMove = false;
    [SerializeField]
    private float duration = 1;
    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (shouldMove && Time.time < timer)
        {
            transform.Translate(-transform.up * Time.deltaTime * 5);
        }

    }

    public void StartMove()
    {
        shouldMove = true;
        timer = Time.time + duration;
    }

}
