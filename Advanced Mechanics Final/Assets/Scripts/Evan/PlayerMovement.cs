﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    //Movement

    private float maxSpeed;
    [Header("Basic Movement")]
    [SerializeField]
    private float defaultMaxSpeed = 15f;
    [SerializeField]
    private float accelTime = 0.1f;
    [SerializeField]
    private float decelTime = 0.2f;


    private Vector3 lastDirection = Vector3.zero;
    private Vector3 currentDirection = Vector3.zero;
    private Vector3 airDirection = Vector3.zero;
    private float currentHorizontalSpeed = 0;
    private bool isDashing = false;
    private float dashTimer;
    [Header("Dashing and Sprinting")]
    [SerializeField]
    private float dashDecelTime = 0.2f;
    private Vector3 dashDirection;
    [SerializeField]
    private float dashTimeInSeconds = 0.5f;
    [SerializeField]
    private float dashingSpeed = 18f;
    [SerializeField]
    private float sprintingSpeed = 12f;
    private float initialHeight;
    private float currentHeight;

    //Crouching AND Sliding
    [Header("Crouching and Sliding")]
    [SerializeField]
    private float crouchHeight = 0.75f;
    [SerializeField]
    private float crouchedCollider = 0.5f;
    [SerializeField]
    private float maxCrouchSpeed;
    private bool crouching = false;
    private bool sliding = false;
    private bool sprintingAndSlidOnce = false;
    [SerializeField]
    private float initialSlidingVelocity = 25f;
    [SerializeField]
    private float slidingTime = 2f;
    private bool isCrouching = false;
    private bool isSliding = false;
    private Vector3 slideDirection;
    [SerializeField]
    private float slideTimeInSeconds = 1f;

    //Jumping
    [Header("Jumping")]
    [SerializeField]
    private float maxJumpHeight = 4f;
    [SerializeField]
    private float minJumpHeight = 1f;
    [SerializeField]
    private float maxJumpTime = 0.4f;
    [SerializeField]
    private float minJumpTime = 0.3f;

    private bool firstJump = false;
    private float currentGravity;

    private float maxGravity;

    private float minGravity;
    [SerializeField]
    private float terminalVelocity = 30f;

    private float bufferJumpTimer = 0;
    [SerializeField]
    private float bufferJumpTime = 0.2f;
    //private float coyoteTimer = 0;
    //[SerializeField]
    //private float coyoteTime = 0.1f;
    //private bool canStartCoyoteTimer = false;
    //private bool justJumped = true;

    private float currentVerticalSpeed = 0;

    //Thrust
    //[Header("Air thrust")]
    //[SerializeField]
    //private float verticalThrust; 
    //private bool canThrust = true;
    //private bool activeThrust = false;
    //private float thrustCapacity = 0;
    //private float thrustCooldownTimer;
    //private bool startedCooldown = false;
    //private bool canRecharge = true;
    //private float maximumThrust = 10;
    //[SerializeField]
    //private float thrustCooldownTime;
    //[SerializeField]
    //private float maximumThrustDuration;

    //Wall Run
    //[Header("Wall Run")]
    //public bool wallDetected = false;
    //private Vector3 alongWall = Vector3.zero;
    //public bool wallExitTracker = false;
    //private float wallRunCooldown = 0.3f;
    //private float wallRunTimer = 0;
    //private RaycastHit lastRayHit;

    //Gunplay
    [Header("Gunplay")]
    [SerializeField]
    private GameObject playerGunSightMarker;
    [SerializeField]
    private GameObject playerGunObject;
    private Vector3 originalGunSightMarkerPos;
    private Quaternion originalGunObjectRot;
    private float defaultFOV = 60;
    [SerializeField]
    private float ADSZoomMultiplier = 0.8333f;

    public GameObject handgun;
    public GameObject ar15;

    [Header("Inventory")]
    [SerializeField]
    private GameObject currentWeapon;
    [SerializeField]
    private GameObject lastWeapon;
    [SerializeField]
    private GameObject heldObject;

    [Header("Misc.")]
    [SerializeField]
    private float groundCastDistance = 0.53f;
    private Rigidbody rigB;

    //Player cam
    public Camera playerCam;

    //Collider
    CapsuleCollider playerCollider;
    float initialColliderHeight;




    private bool Grounded()
    {

        if (Physics.BoxCast(transform.position,
            new Vector3(0.5f, 0.22f, 0.5f),
            -transform.up,
            Quaternion.identity,
            groundCastDistance/* + Mathf.Abs(rigB.velocity.y * Time.deltaTime)*/,
           LayerMask.GetMask("Platform")
            ))
        {


            currentVerticalSpeed = Mathf.Clamp(currentVerticalSpeed, 0, (2 * maxJumpHeight) / maxJumpTime);
            //Debug.Log(currentVerticalSpeed);
            currentGravity = maxGravity;
            //canStartCoyoteTimer = true;
            //justJumped = false;
            //print("JustJumped = false   " + justJumped);
            return true;
        }
        else
        {
            //if (canStartCoyoteTimer && !justJumped)
            //{
            //    coyoteTimer = Time.time + coyoteTime;
            //    canStartCoyoteTimer = false;
            //}
            return false;
        }
    }

    private void Awake()
    {

        rigB = GetComponent<Rigidbody>();
        playerCollider = GetComponent<CapsuleCollider>();

    }

    // Start is called before the first frame update
    void Start()
    {
        //thrustCapacity = maximumThrust;
        //maximumThrustDuration = 0.3f;
        //thrustCooldownTime = 2f;
        //verticalThrust = 2.75f;
        initialColliderHeight = playerCollider.height;
        initialHeight = 1.375f;
        currentHeight = initialHeight;
        //Setting values
        maxSpeed = defaultMaxSpeed;
        maxCrouchSpeed = maxSpeed / 2;
        accelTime = 0.1f;
        decelTime = 0.2f;
        maxJumpHeight = 3.8f;
        maxJumpTime = 0.4f;
        minJumpHeight = 2.5f;
        minJumpTime = 0.3f;
        minGravity = 75f;
        maxGravity = minGravity * (maxJumpHeight / minJumpHeight);
        currentGravity = maxGravity;
        terminalVelocity = 30f;
        dashTimeInSeconds = 0.3f;

        //originalGunSightMarkerPos = playerGunSightMarker.transform.localPosition;//playerCam.WorldToScreenPoint(playerGun.transform.position);
        if (handgun.gameObject.activeSelf)
        {
            playerGunSightMarker = GameObject.Find("AimSightMarkerPistol");
            currentWeapon = handgun;
        }
        else if (ar15.gameObject.activeSelf)
        {
            playerGunSightMarker = GameObject.Find("AimSightMarkerAR15");
            currentWeapon = ar15;
        }
        originalGunSightMarkerPos = playerGunSightMarker.transform.localPosition;
        originalGunObjectRot = playerGunObject.transform.localRotation;
        //Rigid body settings
        rigB.useGravity = false;
        rigB.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        rigB.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

    }


    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Grounded());
        //print(playerCam.WorldToScreenPoint(playerGun.transform.position));

        //MOVEMENT
        //Walking
        CheckWalkingState();

        //Dash
        CheckDashState();
        CheckCrouchNSlidingState();

        //Horizontal speed clamp
        currentHorizontalSpeed = Mathf.Clamp(currentHorizontalSpeed, 0, maxSpeed);


        //JUMPING
        //Initial jump velocity upwards
        CheckJumpState();
        CheckThrustState();

        CheckWallRunState();

        //playerGunSightMarker = GameObject.Find("AimSightMarker");
        AimCheck();

        SwitchWeapons();

        pickUpCheck();
    }

    private void FixedUpdate()
    {
        //print(lastDirection.normalized);
        //print(currentHorizontalSpeed);
        //Vector3 cameraSpaceInput = playerCam.transform.TransformDirection(lastDirection);
        //rigB.position = new Vector3(rigB.position.x, currentHeight, rigB.position.z);
        if (!Grounded())// && !wallDetected)
        {
            Vector3 rigBRelativeInput = rigB.transform.TransformDirection(lastDirection);
            //currentDirection = airDirection;
            currentDirection = rigBRelativeInput;
        }
        else if (Grounded()) {
            Vector3 rigBRelativeInput = rigB.transform.TransformDirection(lastDirection);
            currentDirection = rigBRelativeInput;
        }

        rigB.velocity = (currentDirection.normalized * currentHorizontalSpeed) + new Vector3(0, currentVerticalSpeed, 0);

    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawCube(transform.position, new Vector3(0.22f, -(groundCastDistance + Mathf.Abs(rigB.velocity.y * Time.deltaTime)), 0.22f));
    //}
    //METHODS

    private void pickUpCheck()
    {
        if (InputManager.ActivatePressed())
        {
            RaycastHit hit;
            Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit, 2);

            if (heldObject != null)
            {
                //if (heldObject.tag == "Turret")
                //{
                //    Transform turretPos1 = GameObject.Find("TurretPos1").transform;
                //    Transform turretPos2 = GameObject.Find("TurretPos2").transform;
                //    Transform turretPos3 = GameObject.Find("TurretPos2").transform;
                //    if (Vector3.Distance(heldObject.transform.position, turretPos1.position) < 2)
                //    {
                //        heldObject.transform.position = turretPos1.position;
                //    } else if (Vector3.Distance(heldObject.transform.position, turretPos2.position) < 2)
                //    {
                //        heldObject.transform.position = turretPos2.position;
                //    } else if (Vector3.Distance(heldObject.transform.position, turretPos3.position) < 2)
                //    {
                //        heldObject.transform.position = turretPos3.position;
                //    }
                //}

                heldObject = null;

            } else if (hit.collider.gameObject.GetComponent<Rigidbody>() != null && heldObject == null)
            {
                heldObject = hit.collider.gameObject;
            }


            if (hit.collider.gameObject.name == "StartButton")
            {
                GameObject.Find("Game Manager").GetComponent<GameManager>().StartGame();
            }
        }

        if (heldObject != null)
        {
            heldObject.GetComponent<Rigidbody>().transform.position = playerCam.transform.forward * 1.5f + transform.position;

            //Vector3 lookAt = transform.forward + transform.position;
            //lookAt.y = 0;
            //heldObject.transform.LookAt(lookAt);
        }
        
    }

    private void SwitchWeapons()
    {
        if (Input.GetKeyDown("1") && !ar15.activeSelf)
        {
            lastWeapon = currentWeapon;
            currentWeapon = ar15;
            lastWeapon.gameObject.active = false;
            currentWeapon.gameObject.active = true;
        } else if (Input.GetKeyDown("2") && !handgun.activeSelf)
        {
            lastWeapon = currentWeapon;
            currentWeapon = handgun;
            lastWeapon.gameObject.active = false;
            currentWeapon.gameObject.active = true;
        }
    }

    private void AimCheck()
    {
        if (InputManager.AimHeld())
        {
            if (handgun.gameObject.activeSelf)
            {
                playerGunSightMarker = GameObject.Find("AimSightMarkerPistol");
            }
            else if (ar15.gameObject.activeSelf)
            {
                playerGunSightMarker = GameObject.Find("AimSightMarkerAR15");
            }

            playerGunSightMarker.transform.position = playerCam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0.1f));
            playerGunObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            if (!isDashing)
            {
                playerCam.fieldOfView = defaultFOV * ADSZoomMultiplier;
            }

        }
        else
        {
            if (handgun.gameObject.activeSelf)
            {
                playerGunSightMarker = GameObject.Find("AimSightMarkerPistol");
            }
            else if (ar15.gameObject.activeSelf)
            {
                playerGunSightMarker = GameObject.Find("AimSightMarkerAR15");
            }

            playerGunSightMarker.transform.localPosition = originalGunSightMarkerPos;
            playerGunObject.transform.localRotation = originalGunObjectRot;
            if (!isDashing)
            {
                playerCam.fieldOfView = defaultFOV;
            }

        }
    }

    private bool Moving()
    {
       
        if (InputManager.GetDirectionFromInput() != Vector3.zero)
        {
            return true;
        } else
        {
            return false;
        }
    }

    private void CheckWalkingState() {
        float accel = (defaultMaxSpeed / accelTime) * Time.deltaTime;//TODO move to start when tuned numbers have been selected to avoid multiple calculations
        float decel = (defaultMaxSpeed / decelTime) * Time.deltaTime;//TODO move to start when tuned numbers have been selected to avoid multiple calculations
        if (Moving() && !isDashing && !InputManager.DashHeld())
        {
            lastDirection = InputManager.GetDirectionFromInput();
            if (currentHorizontalSpeed > defaultMaxSpeed && !sliding)
            {
                currentHorizontalSpeed -= decel;
            }
            else //if(!crouching)
            {
                maxSpeed = defaultMaxSpeed;
                currentHorizontalSpeed += accel;
            }

        }
        else if (!Moving() && FinishedDashing())
        {
            isDashing = false;
            maxSpeed = defaultMaxSpeed;
            currentHorizontalSpeed -= decel;
        }

        if (!InputManager.DashHeld()) { sprintingAndSlidOnce = false; }
    }

    private void CheckJumpState() {
        //print(CoyoteAvailable() + "-=-=-=-=-=-=-=-=");
        //print(coyoteTimer + "------");
        //print(justJumped + "++++++");
        //print(!Grounded() && CoyoteAvailable());
        //print(coyoteTimer + "------" + Time.time);
        if (InputManager.JumpPressed() && Grounded() || Grounded() && BufferJumpAvailable())// || InputManager.JumpPressed() && wallDetected) //|| InputManager.JumpPressed() && !Grounded() && CoyoteAvailable())
        {
            //StandUp();
            currentVerticalSpeed += (2 * maxJumpHeight) / maxJumpTime; //TODO move to start when tuned numbers have been selected to avoid multiple calculations
            //if (wallDetected)
            //{
            //    wallDetected = false;
            //    wallRunTimer = Time.time + wallRunCooldown;
            //    //print(transform.TransformPoint(lastRayHit.normal));
            //}
            airDirection = rigB.transform.TransformDirection(lastDirection);
            bufferJumpTimer = 0;
            //justJumped = true;
            //print("JustJumped = true   " + justJumped);
        } else if (InputManager.JumpPressed() && !Grounded())
        {
            bufferJumpTimer = Time.time + bufferJumpTime;
        }

        //Gravity calculation
        float gravityAccel = (maxJumpHeight - minJumpHeight) / (maxJumpTime - minJumpTime); //TODO move to start when tuned numbers have been selected to avoid multiple calculations
        if (InputManager.JumpHeld())// && !activeThrust)
        {
            currentGravity -= gravityAccel;
            currentGravity = Mathf.Clamp(currentGravity, minGravity, maxGravity);
        }

        if (!Grounded() && currentVerticalSpeed > -terminalVelocity)
        {
            currentVerticalSpeed -= currentGravity * Time.deltaTime;
        }

        if (!InputManager.JumpHeld()) {
            //activeThrust = false;
            currentGravity = minGravity;
        }
    }

    private void CheckThrustState() {
        //float rechargeRate = (maximumThrust / maximumThrustDuration) * Time.deltaTime;//TODO move to start when tuned numbers have been selected to avoid multiple calculations
        //if (ThrustCooldownComplete() && canRecharge && !activeThrust) {
        //    thrustCapacity += rechargeRate;
        //   startedCooldown = false; 
           
        //}
        //if (thrustCapacity > 0)
        //{
        //    if (!Grounded() && InputManager.JumpPressed() && !activeThrust)
        //    {
        //        activeThrust = true;
        //        //Debug.Log("activate thrust");
        //    }

        //    if (activeThrust && InputManager.JumpHeld())
        //    {
        //        canRecharge = false;
        //        thrustCapacity -= rechargeRate;
        //        currentVerticalSpeed += verticalThrust;
        //        //maxSpeed = thrustSpeed;
        //        //currentHorizontalSpeed = maxSpeed;
        //        bufferJumpTimer = 0;
        //    }
           
        //}
        //thrustCapacity = Mathf.Clamp(thrustCapacity, 0, maximumThrust);
        //if (thrustCapacity <= 0 && !startedCooldown) {
        //    //Debug.Log("Out of juice");
        //    thrustCooldownTimer = Time.time + thrustCooldownTime;
        //    startedCooldown = true;        
        //}
    }

    private bool ThrustCooldownComplete() {
        //if (Time.time > thrustCooldownTimer) {
        //    canRecharge = true;
        //    //Debug.Log("Ready to recharge");
        //    return true;
        //}        
        return false;
    }
    //private bool CanThrust() {
    //    if(InputManager.JumpHeld() && firstJump)
    //}

    private void CheckDashState()
    {
        //Before actual dash is complete
        if (InputManager.DashPressed() && !isDashing && FinishedDashing())
        {
            dashTimer = Time.time + dashTimeInSeconds;
            isDashing = true;
            dashDirection = InputManager.GetDirectionFromInput();
        }

        if (isDashing && !FinishedDashing())
        {
            maxSpeed = dashingSpeed;
            lastDirection = dashDirection;
            currentHorizontalSpeed = maxSpeed;
            currentVerticalSpeed = 0;
            //playerCam.fieldOfView = defaultFOV * ADSZoomMultiplier;
        }

        //After dash is complete
        if (InputManager.DashHeld() && FinishedDashing())
        {
            lastDirection = InputManager.GetDirectionFromInput();
            //playerCam.fieldOfView = defaultFOV;
            if (!sliding)
            {
                if (currentHorizontalSpeed > sprintingSpeed)
                {
                    //print("TRYING TO SLOW DOWN");
                    float dashDecel = (dashingSpeed / dashDecelTime) * Time.deltaTime;//TODO move to start when tuned numbers have been selected to avoid multiple calculations
                    currentHorizontalSpeed -= dashDecel;
                }
                //else if (crouching)
                //{
                //    StandUp();
                //    maxSpeed = sprintingSpeed;
                //    currentHorizontalSpeed = maxSpeed;
                //}
                else 
                {
                    maxSpeed = sprintingSpeed;
                    currentHorizontalSpeed = maxSpeed;
                }

            }
        }

        if (!InputManager.DashHeld() && FinishedDashing())
        {
            isDashing = false;
            //maxSpeed = defaultMaxSpeed;
            lastDirection = InputManager.GetDirectionFromInput();
            //playerCam.fieldOfView = defaultFOV;
        }
    }

    private bool FinishedDashing()
    {
        if (Time.time > dashTimer)
        {
            return true;
        }

        return false;
    }

    private void CheckCrouchNSlidingState() {

        //Regular crouch
        if (InputManager.CrouchHeld())
        {
            GetComponent<CapsuleCollider>().height = 0.75f;
            groundCastDistance = 0.75f;
            playerCam.transform.localPosition = new Vector3(0, 0.13f, 0);
            isCrouching = true;

        }
        else
        {
            GetComponent<CapsuleCollider>().height = 2.0f;
            groundCastDistance = 1.3f;
            playerCam.transform.localPosition = new Vector3(0, 0.3f, 0);
            isCrouching = false;
        }

        //Slide activation
        //if (InputManager.CrouchHeld() && Grounded() && FinishedDashing() && currentHorizontalSpeed >= sprintingSpeed && !isSliding)
        //{
        //    print("Sliding");
        //    isSliding = true;
        //    slideDirection = InputManager.GetDirectionFromInput();
        //}

        ////Slide
        //if (isSliding && !FinishedSliding())
        //{
        //    print("Decelerating from slide");
        //    float slideDecel = (sprintingSpeed / slideTimeInSeconds) * Time.deltaTime;
        //    lastDirection = slideDirection;
        //    currentHorizontalSpeed -= slideDecel;
        //}

    }

    private bool FinishedSliding()
    {
        if (!InputManager.CrouchHeld() || currentHorizontalSpeed <= defaultMaxSpeed)
        {
            print("Done sliding");
            //isSliding = false;
            return true;
        }

        return false;
    }


    private bool BufferJumpAvailable()
    {
        if (bufferJumpTimer > Time.time)
        {
            return true;
        }

        return false;
    }


    private void CheckWallRunState()
    {
        //float leanDirection = 0;
        ////Debug.DrawRay(rigB.transform.position, playerCam.transform.TransformDirection(InputManager.GetDirectionFromInput().normalized), Color.green);
        //Debug.DrawRay(rigB.transform.position, lastRayHit.normal, Color.green);

        //RaycastHit leftHit, rightHit, inputHit;
        //Physics.Raycast(rigB.transform.position, playerCam.transform.right, out rightHit, currentDirection.magnitude);
        //Physics.Raycast(rigB.transform.position, -playerCam.transform.right, out leftHit, currentDirection.magnitude);
        //Physics.Raycast(rigB.transform.position, playerCam.transform.TransformDirection(InputManager.GetDirectionFromInput().normalized), out inputHit, currentDirection.magnitude);
        //if (!Grounded() && rightHit.collider != null && rightHit.collider != lastRayHit.collider || !Grounded() && rightHit.collider != null && rightHit.collider == lastRayHit.collider && wallRunAvailable())// && Physics.Raycast(rigB.transform.position, rigB.transform.right, out rightHit, currentDirection.magnitude))
        //{
        //    if (Vector3.Dot(rightHit.normal, Vector3.up) == 0 && Vector3.Angle(lastRayHit.normal, rightHit.normal) <= 45)
        //    {
        //        print(Vector3.Dot(lastRayHit.normal, rightHit.normal));
        //        //print("Runnable wall, Baby! R");
        //        lastRayHit = rightHit;
        //        leanDirection = 1;
        //        alongWall = Vector3.Cross(Vector3.up, rightHit.normal);
        //        //Debug.DrawRay(rigB.transform.position, alongWall, Color.green);
        //        wallDetected = true;
        //    }
        //}
        //if (!Grounded() && leftHit.collider != null && leftHit.collider != lastRayHit.collider || !Grounded() && leftHit.collider != null && leftHit.collider == lastRayHit.collider && wallRunAvailable())// && Physics.Raycast(rigB.transform.position, -rigB.transform.right, out leftHit, currentDirection.magnitude))
        //{
        //    if (Vector3.Dot(leftHit.normal, Vector3.up) == 0 && Vector3.Angle(lastRayHit.normal, leftHit.normal) <= 45)
        //    {
        //        print(Vector3.Dot(lastRayHit.normal, leftHit.normal));
        //        //print("Runnable wall, Baby! L");
        //        lastRayHit = leftHit;
        //        leanDirection = -1;
        //        alongWall = Vector3.Cross(leftHit.normal, Vector3.up);
        //        //Debug.DrawRay(rigB.transform.position, alongWall, Color.green);
        //        wallDetected = true;
        //    }
        //}
        //if (!Grounded() && inputHit.collider != null && inputHit.collider != lastRayHit.collider || !Grounded() && inputHit.collider != null && inputHit.collider == lastRayHit.collider && wallRunAvailable())// && Physics.Raycast(rigB.transform.position, rigB.transform.right, out rightHit, currentDirection.magnitude))
        //{
        //    if (Vector3.Dot(inputHit.normal, Vector3.up) == 0)
        //    {
        //        //print("Runnable wall, Baby! I");
        //        lastRayHit = inputHit;
        //        //leanDirection = 1;
        //        //alongWall = Vector3.Cross(Vector3.up, inputHit.normal);
        //        //Debug.DrawRay(rigB.transform.position, alongWall, Color.green);
        //        wallDetected = true;
        //    }
        //}
        //if (leftHit.collider == null && rightHit.collider == null && inputHit.collider == null)
        //{
        //    wallDetected = false;
        //}

        //if (wallDetected && Moving())
        //{
        //    wallExitTracker = true;
        //    currentVerticalSpeed = 0;
        //    maxSpeed = sprintingSpeed;
        //    currentHorizontalSpeed = maxSpeed;

        //    Quaternion angleAlongWall = Quaternion.Euler(alongWall);
        //    currentDirection = alongWall;

        //    Vector3 lookDirection = Vector3.RotateTowards(rigB.transform.forward, alongWall, 2 * Mathf.Deg2Rad, 0.0f);
        //    rigB.transform.rotation = Quaternion.LookRotation(lookDirection);


        //    LeanTween.rotateZ(playerCam.gameObject, 12.5f * leanDirection, 0.1f);
        //}


        //if (!wallDetected && wallExitTracker)
        //{
        //    //Just left wall
        //    wallExitTracker = false;
        //    LeanTween.rotateZ(playerCam.gameObject, 0, 0.1f);

        //    Quaternion camRotation = playerCam.transform.rotation;

        //    Vector3 lookPos = playerCam.transform.forward;
        //    lookPos.y = rigB.transform.forward.y;
        //    rigB.transform.rotation = Quaternion.LookRotation(lookPos);

        //    playerCam.transform.rotation = camRotation;

        //}

    }
        

    private bool wallRunAvailable()
    {
        //if (wallRunTimer > Time.time)
        //{
        //    return false;
        //}

        return true;
    }
    
}
