﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleShoot : MonoBehaviour
{

    public GameObject bulletPrefab;
    public GameObject casingPrefab;
    public GameObject muzzleFlashPrefab;
    public Transform barrelLocation;
    public Transform casingExitLocation;

    public bool isAr15 = false;
    public bool isHandgun = true;

    public float shotDelay = 0.1f;
    public float shotDelayTimer;
    public float shotPower = 100f;

    void Start()
    {
        if (barrelLocation == null)
            barrelLocation = transform;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && isHandgun && shotDelayTimer < Time.time)
        {
            GetComponent<Animator>().SetTrigger("Fire");
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
            shotDelayTimer = Time.time + shotDelay;
        } else if (Input.GetButton("Fire1") && isAr15 && shotDelayTimer < Time.time)
        {
            Shoot();
            CasingRelease();
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
            shotDelayTimer = Time.time + shotDelay;
        }
    }

    void Shoot()
    {
        //  GameObject bullet;
        //  bullet = Instantiate(bulletPrefab, barrelLocation.position, barrelLocation.rotation);
        // bullet.GetComponent<Rigidbody>().AddForce(barrelLocation.forward * shotPower);

        GameObject tempFlash;
        GameObject bulletClone;
        bulletClone = Instantiate(bulletPrefab, barrelLocation.position, barrelLocation.rotation);
        bulletClone.GetComponent<Rigidbody>().AddForce(barrelLocation.forward * shotPower);
        if (isHandgun)
        {
            bulletClone.GetComponent<BulletScript>().damage = 4;
        }
        tempFlash = Instantiate(muzzleFlashPrefab, barrelLocation.position, barrelLocation.rotation);

        Destroy(tempFlash, 0.5f);
        Destroy(bulletClone, 0.8f);
        //  Instantiate(casingPrefab, casingExitLocation.position, casingExitLocation.rotation).GetComponent<Rigidbody>().AddForce(casingExitLocation.right * 100f);
       
    }

    void CasingRelease()
    {
         GameObject casing;
        casing = Instantiate(casingPrefab, casingExitLocation.position + Vector3.up * 0.05f + Vector3.right * 0.05f, casingExitLocation.rotation) as GameObject;
        casing.GetComponent<Rigidbody>().AddExplosionForce(550f, (casingExitLocation.position - casingExitLocation.right * 0.5f - casingExitLocation.up * 0.6f), 1f);
        casing.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(100f, 500f), Random.Range(10f, 1000f)), ForceMode.Impulse);

        Destroy(casing, 3);
    }


}
