﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public int damage = 1;
    [SerializeField]
    private GameObject onHitEffect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Damage
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyScript>().TakeDamage(damage);
            
        }

        GameObject onHit = Instantiate(onHitEffect, transform) as GameObject;
        Destroy(onHit, 0.25f);

        //Destroy(this.gameObject, 0.5f);
        Destroy(this.gameObject);
    }

}
