﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    [SerializeField]
    private bool shouldMove = true;
    public float moveSpeed = 3;
    [SerializeField]
    private float health = 8;
    [SerializeField]
    private bool giveScore = true;

    [SerializeField]
    private GameObject GameManager;
    Rigidbody rigB;
    [SerializeField]
    private GameObject body;
    private float flashTimer = 0;
    private float flashDuration = 0.08f;

    // Start is called before the first frame update
    void Start()
    {

        rigB = GetComponent<Rigidbody>();
        GameManager = GameObject.Find("Game Manager");

    }

    // Update is called once per frame
    void Update()
    {
        
        if (shouldMove)
        {
            rigB.velocity = transform.forward * moveSpeed;
        }

        if (flashTimer > Time.time)
        {
            body.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Color", Color.red);
        } else
        {
            body.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Color", Color.white);
        }

    }

    public void TakeDamage(int amount)
    {
        health -= amount;

        flashTimer = Time.time + flashDuration;

        if (health <= 0)
        {
            if (giveScore)
            {
                GameManager.GetComponent<GameManager>().score++;
            }
            
            Destroy(this.gameObject);
        }
    }

}
