﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class GameManager : MonoBehaviour
{

    [Header("Start Game")]
    [SerializeField]
    private GameObject blockOffWall;
    [SerializeField]
    private GameObject text1, text2, text3, text4;
    [SerializeField]
    private GameObject startButton;
    [SerializeField]
    private GameObject scoreDisplay;
    [SerializeField]
    private GameObject windowHealthDisplay;
    private TextMesh scoreText, windowHealthText;

    [Header("Enemy Spawning")]
    public bool shouldSpawnEnemies;
    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private Transform enemySpawn1, enemySpawn2, enemySpawn3;
    [SerializeField]
    private float spawnDelay = 3;
    private float spawnTimer;
    [SerializeField]
    private float spawnDelayDecrease = 0.01f;
    [SerializeField]
    private float speedIncrease = 0.1f;
    private float totalSpeed;

    [Header("Traps")]
    [SerializeField]
    private GameObject plate1;
    [SerializeField]
    private GameObject plate2, plate3;

    [Header("Score")]
    public float score = 0;
    public float windowHealth = 100;

    [Header("Audio")]
    [SerializeField]
    private GameObject calmMusicManager;
    private StudioEventEmitter calmMusicEmitter;
    [SerializeField]
    private GameObject fightMusicManager;
    private StudioEventEmitter fightMusicEmitter;

    // Start is called before the first frame update
    void Start()
    {

        scoreText = scoreDisplay.GetComponent<TextMesh>();
        windowHealthText = windowHealthDisplay.GetComponent<TextMesh>();
        calmMusicEmitter = calmMusicManager.GetComponent<StudioEventEmitter>();
        fightMusicEmitter = fightMusicManager.GetComponent<StudioEventEmitter>();

        totalSpeed = 3;

    }

    // Update is called once per frame
    void Update()
    {
        //print(spawnTimer);
        if (shouldSpawnEnemies && spawnTimer < Time.time)
        {
            SpawnEnemies();
            totalSpeed += speedIncrease;
            spawnDelay -= spawnDelayDecrease;
            spawnDelay = Mathf.Clamp(spawnDelay, 0, 10);
        }

        scoreText.text = "Score: " + score;
        windowHealthText.text = "Window Health: " + windowHealth;
    }

    private void SpawnEnemies()
    {
        float selectedSpawn = Random.Range(1, 4);
        if (selectedSpawn == 1)
        {
            GameObject enemyClone = Instantiate(enemy, enemySpawn1.position, enemySpawn1.rotation);
            enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed;
            if (plate3.GetComponent<SlowPlate>().activated)
            {
                enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed / 2;
            }
            spawnTimer = Time.time + spawnDelay;
        } else if (selectedSpawn == 2)
        {
            GameObject enemyClone = Instantiate(enemy, enemySpawn2.position, enemySpawn2.rotation);
            enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed;
            if (plate2.GetComponent<SlowPlate>().activated)
            {
                enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed / 2;
            }
            spawnTimer = Time.time + spawnDelay;
        } else if (selectedSpawn == 3)
        {
            GameObject enemyClone = Instantiate(enemy, enemySpawn3.position, enemySpawn3.rotation);
            enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed;
            if (plate1.GetComponent<SlowPlate>().activated)
            {
                enemyClone.GetComponent<EnemyScript>().moveSpeed = totalSpeed / 2;
            }
            spawnTimer = Time.time + spawnDelay;
        }
    }

    public void StartGame()
    {
        blockOffWall.active = true;
        shouldSpawnEnemies = true;
        text1.active = false;
        text2.active = false;
        text3.active = false;
        text4.active = false;
        scoreDisplay.active = true;
        windowHealthDisplay.active = true;
        startButton.active = false;

        calmMusicEmitter.Stop();
        fightMusicEmitter.Play();
        //musicEmitter.Stop();
        //musicEmitter.SetParameter("State", 1);
        //musicEmitter.Play();
    }
}
