﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAI : MonoBehaviour
{

    [SerializeField]
    private bool shouldShoot;
    [SerializeField]
    private bool shouldAim;
    private GameObject target;

    public GameObject bulletPrefab;
    public GameObject casingPrefab;
    public GameObject muzzleFlashPrefab;
    public Transform barrelLocation;
    public Transform casingExitLocation;

    public float shotDelay = 0.1f;
    public float shotDelayTimer;
    public float shotPower = 100f;

    public int damage = 1;
    public int powerLevel = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        powerLevel = Mathf.Clamp(powerLevel, 0, 10);

        //damage = powerLevel;
        shotDelay = 1.0f / powerLevel;

        if (shouldShoot || powerLevel >= 1 && shotDelayTimer < Time.time)
        {
            Shoot();
            CasingRelease();
            shotDelayTimer = Time.time + shotDelay;
        }

        if (target != null && shouldAim)
        {
            transform.LookAt(target.transform);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (target == null && other.tag == "Enemy")
        {
            target = other.gameObject;
        }
    }

    void Shoot()
    {

        GameObject tempFlash;
        GameObject bulletClone;
        bulletClone = Instantiate(bulletPrefab, barrelLocation.position, barrelLocation.rotation);
        bulletClone.GetComponent<Rigidbody>().AddForce(barrelLocation.forward * shotPower);
        bulletClone.GetComponent<BulletScript>().damage = damage;
        tempFlash = Instantiate(muzzleFlashPrefab, barrelLocation.position, barrelLocation.rotation);

        GetComponent<FMODUnity.StudioEventEmitter>().Play();

        Destroy(tempFlash, 0.5f);
        Destroy(bulletClone, 0.8f);

    }

    void CasingRelease()
    {
        GameObject casing;
        casing = Instantiate(casingPrefab, casingExitLocation.position + Vector3.up * 0.05f + Vector3.right * 0.05f, casingExitLocation.rotation) as GameObject;
        casing.GetComponent<Rigidbody>().AddExplosionForce(550f, (casingExitLocation.position - casingExitLocation.right * 0.5f - casingExitLocation.up * 0.6f), 1f);
        casing.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(100f, 500f), Random.Range(10f, 1000f)), ForceMode.Impulse);

        Destroy(casing, 3);
    }

}
