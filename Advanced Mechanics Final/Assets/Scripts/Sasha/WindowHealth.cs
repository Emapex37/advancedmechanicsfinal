﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class WindowHealth : MonoBehaviour
{
    Renderer rend;
    [SerializeField]
    float hitPoints;
    [SerializeField]
    private GameObject GameManager;

    private float flashTimer = 0;
    private float flashDuration = 0.1f;

    private void Awake()
    {
        rend = GetComponent<Renderer>(); 
    }

    // Start is called before the first frame update
    void Start()
    {
        rend.material.color = new Color(0, 25, 255);

        hitPoints = 100; 
    }

    // Update is called once per frame
    void Update()
    {
        if (hitPoints <= 0)
        {
            rend.material.color = new Color(255, 0, 0);
            StartCoroutine(endOfZaWarudo()); 
        }

        if (flashTimer > Time.time)
        {
            rend.material.color = new Color(255, 0, 0);
        } else
        {
            rend.material.color = new Color(0, 25, 255);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            hitPoints -=10;
            flashTimer = Time.time + flashDuration;
            GameManager.GetComponent<GameManager>().windowHealth = hitPoints;

            Destroy(collision.gameObject);
        }
    }

    private IEnumerator endOfZaWarudo()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("GameOver"); 

    }
}
